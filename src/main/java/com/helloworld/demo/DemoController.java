package com.helloworld.demo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class DemoController {
    @GetMapping("/demo-app/hello")
    public String hello() {
        return "Hello, World!";
    }
}
