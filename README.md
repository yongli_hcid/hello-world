=== DONT'T PUSH ANYTHING TO THIS REPOSITORY ===

Setup Guide :

1. Clone this project
2. install needed dependency using proper java version (you can use v15 or use your own version)
   3. you can use `mvn clean install`
4. test running locally
   5. use this command ` java -cp target/classes:target/lib/* com.helloworld.demo.DemoApplication
      `
6. Open your browser and open url `http://localhost:8080/demo-app/hello`
   (Note : Application is using port 8080. DONT CHANGE THIS)


Your task : 
1. Containerize this project using docker
   2. Create Dockerfile
3. Create kubernetes deployment yaml for this project

Objective :
1. Create needed file (ex: Dockerfile, kubernetes yaml)
2. Create text file containing the command for build, run / deploy the container (ex: tutorial.txt)
3. Able to run browser this project using port 80 instead of 8080, example `localhost/demo-app/hello`
   (Note dont change port directly in code. Use docker / kubernetes to handle this)
   Take screenshot of the browser that opening `localhost/demo-app/hello`

Send all new file created (dockerfile, kubernetes yaml, tutorial.txt, screenshot) to hiring team

